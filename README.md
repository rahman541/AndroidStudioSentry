## Testing Android studio with Sentry.io
## How to retrieve key
1. Project Setting
![alt text](readme/1st.png)

2. Client Keys (DSN)
![alt text](readme/2nd.png)

3. Copy DSN
![alt text](readme/3rd.png)
